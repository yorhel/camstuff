#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <time.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/sctp.h>
#include <pthread.h>
#include <libexif/exif-data.h>

#define DEBUG true
#define HEADER_SIZE (8*3)
#define MAX_FRAME_SIZE (256*1024)


// Buffers and info broadcasted to streaming threads.
// Frame buffers get copied rather more often than necessary, but it does the trick.
static char stream_buf[MAX_FRAME_SIZE]; // last received frame
static int stream_size; // last received frame size
static uint64_t stream_ts; // last received frame timestamp
static int stream_listeners; // number of listeners on the stream
static pthread_mutex_t stream_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t stream_cond = PTHREAD_COND_INITIALIZER;


static int sock;

static uint64_t oldest_frame; // Oldest frame we've not yet received & acknowledged.


#define TS2PATH_BUF 28 // YYYY-MM/DD/HH/mmmss.mss.jpg
static char *ts2path(char *buf, uint64_t ts, bool create) {
    time_t time = ts/1000;
    struct tm tm;
    gmtime_r(&time, &tm);
    strftime(buf, 8, "%Y-%m", &tm);
    if (create) mkdir(buf, 0777);
    buf[7] = '/';
    strftime(buf+8, 3, "%d", &tm);
    if (create) mkdir(buf, 0777);
    buf[10] = '/';
    strftime(buf+11, 3, "%H", &tm);
    if (create) mkdir(buf, 0777);
    buf[13] = '/';
    strftime(buf+14, 7, "%Mm%S.", &tm);
    snprintf(buf+20, 8, "%03d.jpg", (int)(ts%1000));
    return buf;
}

static uint64_t path2ts(char *buf) {
    struct tm tm;
    int msec;
    if (sscanf(buf, "%d-%d/%d/%d/%dm%d.%d.jpg", &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour, &tm.tm_min, &tm.tm_sec, &msec) < 7)
        return 0;
    tm.tm_year -= 1900;
    tm.tm_mon -= 1;
    tm.tm_isdst = -1;
    time_t t = timegm(&tm); // glibc thing
    if (t < 0) return 0;
    return msec + ((uint64_t)t) * 1000;
}

static uint64_t ts_now() {
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);
    return (uint64_t)t.tv_sec * 1000 + (uint64_t)t.tv_nsec/1000000;
}

static void *stream_sender(void *x) {
    pthread_mutex_lock(&stream_mutex);
    stream_listeners++;
    pthread_mutex_unlock(&stream_mutex);

    int s = (intptr_t)x;
    char buf[1024+MAX_FRAME_SIZE];
    int buflen;
    uint64_t lastframe = 0;
    const char *header = "HTTP/1.0 200 OK\r\nContent-Type: multipart/x-mixed-replace; boundary=\"exqGwlGsXX0SQIuH5tozuAAwvzrvZSw4\"\r\n";
    const char *frame_header = "\r\n--exqGwlGsXX0SQIuH5tozuAAwvzrvZSw4\r\nContent-type: image/jpeg\r\nContent-length: %d\r\n\r\n";

    // Just assume we get some sort of HTTP request, no need to bother doing anything with it.
    if (read(s, buf, sizeof(buf)) <= 0) goto end;

    strcpy(buf, header);
    buflen = strlen(header);

    for (;;) {
        while (buflen > 0) {
            int n = write(s, buf, buflen);
            if (n <= 0) goto end;
            buflen -= n;
            memmove(buf, buf+n, buflen);
        }
        pthread_mutex_lock(&stream_mutex);
        while (stream_ts == lastframe) pthread_cond_wait(&stream_cond, &stream_mutex);
        lastframe = stream_ts;
        buflen = sprintf(buf, frame_header, stream_size);
        memcpy(buf+buflen, stream_buf, stream_size);
        buflen += stream_size;
        pthread_mutex_unlock(&stream_mutex);
    }

end:
    pthread_mutex_lock(&stream_mutex);
    stream_listeners--;
    if (stream_listeners == 0) stream_ts = 0;
    pthread_mutex_unlock(&stream_mutex);
    return NULL;
}


static void *stream_listener(void *x) {
    int s = *(int *)x;
    for (;;) {
        int fd = accept(s, NULL, NULL);
        if (fd < 0) {
            fprintf(stderr, "Error accepting TCP connection: %s\n", strerror(errno));
            sleep(1);
            continue;
        }
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_t pid;
        pthread_create(&pid, &attr, stream_sender, (void *)(intptr_t)fd);
    }
    return NULL;
}


// Adapted from https://github.com/libexif/libexif/blob/master/contrib/examples/write-exif.c
static ExifEntry *exif_init_tag(ExifData *exif, ExifIfd ifd, ExifTag tag) {
    ExifEntry *entry;
    if (!((entry = exif_content_get_entry(exif->ifd[ifd], tag)))) {
        entry = exif_entry_new();
        entry->tag = tag;
        exif_content_add_entry(exif->ifd[ifd], entry);
        exif_entry_initialize(entry, tag);
        exif_entry_unref(entry);
    }
    return entry;
}

static ssize_t exif_write(char *inbuf, ssize_t inlen, uint64_t ts, char *outbuf) {
    unsigned char *exif = NULL;

    // let's be nice and extract proper image dimensions for EXIF
    ssize_t off = 0;
    uint16_t width = 0, height = 0;
    while (off+4 < inlen) {
        if ((uint8_t)inbuf[off++] != 0xff) break;
        uint8_t mark = inbuf[off++];
        if ((mark == 0xc0 || mark == 0xc2) && off > 8) { // frame segment
            height = (((uint8_t)inbuf[off+3])<<8) + (uint8_t)inbuf[off+4];
            width = (((uint8_t)inbuf[off+5])<<8) + (uint8_t)inbuf[off+6];
            break;
        } else {
            off += (uint8_t)inbuf[off] == 0xff ? 0 : (((uint8_t)inbuf[off])<<8) + (uint8_t)inbuf[off+1];
        }
    }

    ExifData *ex = exif_data_new_from_data(inbuf, inlen);
    if (!ex) goto error;
    exif_data_set_option(ex, EXIF_DATA_OPTION_IGNORE_UNKNOWN_TAGS);
    exif_data_set_data_type(ex, EXIF_DATA_TYPE_COMPRESSED);
    exif_data_set_byte_order(ex, EXIF_BYTE_ORDER_MOTOROLA);
    exif_data_fix(ex);
    exif_set_long(exif_init_tag(ex, EXIF_IFD_EXIF, EXIF_TAG_PIXEL_X_DIMENSION)->data, EXIF_BYTE_ORDER_MOTOROLA, width);
    exif_set_long(exif_init_tag(ex, EXIF_IFD_EXIF, EXIF_TAG_PIXEL_Y_DIMENSION)->data, EXIF_BYTE_ORDER_MOTOROLA, height);
    exif_set_short(exif_init_tag(ex, EXIF_IFD_EXIF, EXIF_TAG_COLOR_SPACE)->data, EXIF_BYTE_ORDER_MOTOROLA, 1);

    time_t time = ts/1000;
    struct tm tm;
    localtime_r(&time, &tm); // EXIF doesn't specify a timezone, but local time is common and what we want in our video timestamp
    strftime(exif_init_tag(ex, EXIF_IFD_0, EXIF_TAG_DATE_TIME)->data, 20, "%Y-%m-%d %H:%M:%S", &tm);

    unsigned int exiflen = 0;
    exif_data_save_data(ex, &exif, &exiflen);
    exif_data_free(ex);
    if (exiflen == 0 || exiflen > MAX_FRAME_SIZE-128) goto error;

    // Write JPEG header & EXIF data
    memcpy(outbuf, "\xff\xd8\xff\xe1", 4);
    outbuf[4] = (2+exiflen) >> 8;
    outbuf[5] = (2+exiflen) & 255;
    memcpy(outbuf+6, exif, exiflen);

    // Now write the rest of the JPEG file, removing APP1 segments.
    ssize_t outoff = 6 + exiflen;
    off = 2;
    while (off+4 < inlen) {
        if ((uint8_t)inbuf[off] != 0xff) goto error;
        uint8_t mark = inbuf[off+1];
        uint16_t len = 2 + ((uint8_t)inbuf[off+2] == 0xff ? 0 : (((uint8_t)inbuf[off+2])<<8) + (uint8_t)inbuf[off+3]);
        if (mark != 0xe1) {
            if (outoff + len > MAX_FRAME_SIZE) goto error;
            memcpy(outbuf+outoff, inbuf + off, len);
            outoff += len;
        }
        off += len;
        if (mark == 0xda) { // SOF, what follows is entropy-coded data
            while (off < inlen) {
                if ((uint8_t)inbuf[off] == 0xff && off+1 < inlen && !(inbuf[off+1] == 0 || (((uint8_t)inbuf[off+1])&0xf0) == 0xd0)) break;
                outbuf[outoff++] = inbuf[off++];
            }
        }
    }
    free(exif);
    return outoff;

error:
    free(exif);
    memcpy(outbuf, inbuf, inlen);
    return inlen;
}


static bool handlemsg(char *buf, ssize_t buflen) {
    uint64_t ts;
    uint64_t oldest_ts;
    memcpy(&ts,        buf+ 8, 8);
    memcpy(&oldest_ts, buf+16, 8);

    char path[TS2PATH_BUF];
    ts2path(path, ts, true);
    if (ts < oldest_frame) {
        fprintf(stderr, "Discarding frame with too old timestamp %s\n", path);
        return true;
    }

    char img[MAX_FRAME_SIZE];
    ssize_t imglen = exif_write(buf+HEADER_SIZE, buflen-HEADER_SIZE, ts, img);

    int fd = open(path, O_WRONLY|O_CREAT|O_EXCL, 0666);
    if (fd < 0 && errno == EEXIST) {
        fprintf(stderr, "Discarding duplicate frame %s\n", path);
        return true;
    } else if (fd < 0) {
        fprintf(stderr, "Error creating %s: %s\n", path, strerror(errno));
        return false;
    }

    ssize_t off = 0;
    while (off < imglen) {
        ssize_t x = write(fd, img+off, imglen-off);
        if (x <= 0) {
            fprintf(stderr, "Error writing to %s: %s\n", path, strerror(errno));
            close(fd);
            return false;
        }
        else off += x;
    }
    close(fd);

    if (oldest_ts > oldest_frame) {
        oldest_frame = oldest_ts;
        char latestpath[TS2PATH_BUF];
        ts2path(latestpath, oldest_frame, false);
        if (ts == oldest_frame || access(latestpath, F_OK) == 0) {
            unlink("latest.jpg");
            symlink(latestpath, "latest.jpg");
        }
    }

    pthread_mutex_lock(&stream_mutex);
    if (stream_listeners) {
        stream_ts = ts;
        stream_size = imglen;
        memcpy(stream_buf, img, imglen);
        pthread_cond_broadcast(&stream_cond);
    }
    pthread_mutex_unlock(&stream_mutex);

    DEBUG && fprintf(stderr, "Received frame %s (%.1f KiB, %.1fs old, %.1fs backlog)\n", path,
            (double)(imglen)/1024,
            (double)((int64_t)ts_now()-(int64_t)ts)/1000,
            (double)((int64_t)ts_now()-(int64_t)oldest_ts)/1000);
    return true;
}


int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <path> <port>\n", argv[0]);
        return 1;
    }
    // chdir, so we can be lazy and use relative paths everywhere.
    if (chdir(argv[1]) != 0) {
        fprintf(stderr, "Unable to enter '%s': %s\n", argv[1], strerror(errno));
        return 1;
    }

    char pathbuf[TS2PATH_BUF];
    if (readlink("latest.jpg", pathbuf, TS2PATH_BUF) == TS2PATH_BUF-1) {
        pathbuf[TS2PATH_BUF-1] = 0;
        oldest_frame = path2ts(pathbuf);
        DEBUG && fprintf(stderr, "Latest frame on disk: %s (%"PRIu64")\n", pathbuf, oldest_frame);
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(atoi(argv[2]));

    if ((sock = socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP)) < 0) {
        fprintf(stderr, "Unable to create socket: %s\n", strerror(errno));
        return 1;
    }

    if (bind(sock, (struct sockaddr*)&addr, sizeof(addr)) < 0 || listen(sock, 50) < 0) {
        fprintf(stderr, "Unable to bind socket: %s\n", strerror(errno));
        return 1;;
    }

    int streamsock = socket(PF_INET, SOCK_STREAM, 0);
    if (streamsock < 0
            || bind(streamsock, (struct sockaddr*)&addr, sizeof(addr)) < 0
            || listen(streamsock, 5) < 0) {
        fprintf(stderr, "Unable to create TCP socket: %s\n", strerror(errno));
        return 1;
    }

    signal(SIGPIPE, SIG_IGN);
    pthread_t pid;
    pthread_create(&pid, NULL, stream_listener, &streamsock);

    for (;;) {
        char buf[HEADER_SIZE + MAX_FRAME_SIZE];
        struct sockaddr_in from;
        socklen_t fromlen = sizeof(from);
        ssize_t off = 0;
        for (;;) {
            struct sctp_sndrcvinfo sinfo = {};
            int flags = 0;
            ssize_t r = sctp_recvmsg(sock, buf+off, sizeof(buf)-off, (struct sockaddr*)&from, &fromlen, &sinfo, &flags);
            if (r < 0) {
                off = r;
                break;
            }
            off += r;
            if (flags & MSG_EOR) break;
        }
        if (off < HEADER_SIZE) {
            fprintf(stderr, "Error receiving message: %s\n", strerror(errno));
            sleep(1);
        } else if (handlemsg(buf, off))
            sctp_sendmsg(sock, buf, 8, (struct sockaddr *)&from, fromlen, 0, SCTP_UNORDERED, 0, 20*1000, 0);
    }
}
