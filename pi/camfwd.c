#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <pthread.h>

#include "khashl.h"

#define DEBUG false
#define HEADER_SIZE (8*3)
#define MAX_FRAME_SIZE (256*1024)
#define RESEND_INTERVAL_SEC 60
#define READDIR_SKIP_QUEUE_SIZE 100 /* Stop reading more frames when we have this many queued */
#define READDIR_INTERVAL_MSEC 50


typedef struct Frame {
    char *fn;
    uint64_t ino;
    uint64_t tm;
    int size;
    struct timespec queued;
    struct Frame *next, *prev;
} Frame;

KHASHL_MAP_INIT(KH_LOCAL, FrameIno, fi, uint64_t, Frame*, kh_hash_uint64, kh_eq_generic);

static Frame *fifo_head, *fifo_tail;
static FrameIno *frame_inos;

// Mutex protects the above hashmap and queue, but also synchronizes filesystem
// access, in particular to ensure we don't add a new frame to the queue when
// it's just been deleted concurrently by an ack from the server.
static pthread_mutex_t frame_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t frame_cond = PTHREAD_COND_INITIALIZER; // When a new frame is added to the queue

static struct sockaddr_in servaddr;
static int sock;

static int framecmp(const void *a, const void *b) {
    Frame *fa = *((Frame **)a), *fb = *((Frame **)b);
    return fa->tm > fb->tm ? 1 : fa->tm < fb->tm ? -1 : 0;
}

// UNIX timestamp, but in milliseconds
static uint64_t time2ts(struct timespec tm) {
    return (uint64_t)tm.tv_sec * 1000 + (uint64_t)tm.tv_nsec/1000000;
}


static void scanframes() {
    pthread_mutex_lock(&frame_mutex);
    if (kh_size(frame_inos) >= READDIR_SKIP_QUEUE_SIZE) {
        pthread_mutex_unlock(&frame_mutex);
        return;
    }

    DIR *d = opendir(".");
    if (d == NULL) {
        fprintf(stderr, "Error opening dir: %s\n", strerror(errno));
        exit(1); // I mean, this shouldn't happen, so better let monitoring notice we've stopped running.
    }

    struct timespec _now;
    clock_gettime(CLOCK_REALTIME, &_now);
    uint64_t now = time2ts(_now);

    struct dirent *e;
    Frame **toadd = NULL;
    size_t toadd_size = 0, toadd_len = 0;
    while ((e = readdir(d)) != NULL) {
        uint64_t ino = e->d_ino;
        if (e->d_type != DT_REG) continue;
        bool dup = fi_get(frame_inos, ino) != kh_end(frame_inos);
        if (dup) continue;

        struct stat st;
        if (stat(e->d_name, &st) < 0) continue;
        if (st.st_size > MAX_FRAME_SIZE) continue;
        uint64_t mtime = time2ts(st.st_mtim);
        // Give the frame writing process some time to settle writing the files.
        // We don't really have a way to know when it's finished, sadly.
        if (mtime <= now && (now - mtime) < 100) continue;

        Frame *f = calloc(1, sizeof(Frame));
        f->fn = strdup(e->d_name);
        f->ino = ino;
        f->tm = mtime;
        f->size = st.st_size;

        if (toadd_len == toadd_size) {
            if (toadd_size) toadd_size *= 2;
            else toadd_size = 32;
            toadd = realloc(toadd, toadd_size*sizeof(Frame *));
        }
        toadd[toadd_len++] = f;
    }
    closedir(d);

    // Sort by date before adding to the queue, so that older frames are added first.
    // (Assumption: frames found in subsequent reads will have a more recent timestamp)
    qsort(toadd, toadd_len, sizeof(Frame *), framecmp);

    for (size_t i=0; i<toadd_len; i++) {
        Frame *f = toadd[i];
        DEBUG && fprintf(stderr, "New frame: %9llx %9llub  %llu.%03llu  %s\n",
                (long long unsigned)f->ino, (long long unsigned)f->size,
                (long long unsigned)f->tm/1000, (long long unsigned)f->tm%1000, f->fn);
        f->prev = fifo_tail;
        if (fifo_tail) fifo_tail->next = f;
        fifo_tail = f;
        if (!fifo_head) fifo_head = f;

        khint_t absent;
        khint_t idx = fi_put(frame_inos, f->ino, &idx);
        kh_val(frame_inos, idx) = f;
    }

    if (toadd_len) pthread_cond_signal(&frame_cond);
    pthread_mutex_unlock(&frame_mutex);
    free(toadd);
}


// Reset the 'queued' field if the frame has been queued more than
// RESEND_INTERVAL_SEC seconds. SCTP should take care of resending packets if
// there's packet loss or other network-related issues, but frames can still
// get lost if the SCTP association is temporarily gone(?) or if the server has
// crashed/is stopped before the frame has been saved, so we'll need to attempt
// a resend of older frames either way.  (This can be done a lot faster by
// moving 'queued' files to a separate linked list)
void resetqueued() {
    pthread_mutex_lock(&frame_mutex);

    size_t unsent = 0;
    struct timespec tm;
    clock_gettime(CLOCK_MONOTONIC, &tm);
    for (Frame *f=fifo_head; f; f=f->next) {
        // Imprecise comparison, w/e
        if (f->queued.tv_sec > 0 && f->queued.tv_sec + RESEND_INTERVAL_SEC < tm.tv_sec) {
            DEBUG && fprintf(stderr, "No ack before the resend timeout for %s\n", f->fn);
            f->queued.tv_sec = 0;
        }
        if (f->queued.tv_sec == 0) unsent++;
    }

    if (unsent) pthread_cond_signal(&frame_cond);
    pthread_mutex_unlock(&frame_mutex);
}


void *sender(void *x) {
    // Message format:
    //   uint64_t this_frame_ino;
    //   uint64_t this_frame_timestamp;
    //   uint64_t oldest_frame_timestamp;
    //   char[] frame_body;
    char buf[HEADER_SIZE + MAX_FRAME_SIZE];

    pthread_mutex_lock(&frame_mutex);
    for (;;) {
        Frame *f;
        for (;;) {
            for (f=fifo_head; f; f=f->next) if (f->queued.tv_sec == 0) break;
            if (f) break;
            pthread_cond_wait(&frame_cond, &frame_mutex);
        }
        memcpy(buf+ 0, &f->ino, 8);
        memcpy(buf+ 8, &f->tm, 8);
        memcpy(buf+16, &fifo_head->tm, 8);
        int off = HEADER_SIZE;
        int size = f->size;

        uint64_t ino = f->ino;
        int fd = open(f->fn, O_RDONLY);
        if (fd < 0) {
            fprintf(stderr, "Unable to open '%s': %s\n", f->fn, strerror(errno));
            exit(1); // XXX: better to remove file from queue and handle this nicely.
        }
        pthread_mutex_unlock(&frame_mutex);

        while (size > 0) {
            int rd = read(fd, buf+off, size);
            if (rd <= 0) {
                fprintf(stderr, "Unable to read file: %s\n", strerror(errno));
                exit(1); // XXX
            }
            off += rd;
            size -= rd;
        }
        close(fd);

        int r = sctp_sendmsg(sock, buf, off, (struct sockaddr *)&servaddr, sizeof(servaddr), 0, SCTP_UNORDERED, 0, RESEND_INTERVAL_SEC*1000, 0);
        // Let the 'queue' retransmission thing handle the retrying of this frame, no special handling required here.
        if (r != off) fprintf(stderr, "Unable to send message: %s\n", strerror(errno));

        pthread_mutex_lock(&frame_mutex);

        // Make sure to validate our 'f' pointer as it could've been freed.
        khint_t idx = fi_get(frame_inos, ino);
        if (idx != kh_end(frame_inos) && kh_val(frame_inos, idx) == f) {
            DEBUG && fprintf(stderr, "Wrote '%s' to the kernel buffer\n", f->fn);
            clock_gettime(CLOCK_MONOTONIC, &f->queued);
        }
    }
    pthread_mutex_unlock(&frame_mutex);
    return NULL;
}


void *receiver(void *x) {
    for (;;) {
        uint64_t ino;
        if (recv(sock, &ino, 8, 0) != 8) {
            fprintf(stderr, "Error receiving message: %s\n", strerror(errno));
            sleep(1);
            continue;
        }
        pthread_mutex_lock(&frame_mutex);
        khint_t idx = fi_get(frame_inos, ino);
        if (idx != kh_end(frame_inos)) {
            Frame *f = kh_val(frame_inos, idx);
            if (f->next) f->next->prev = f->prev;
            if (f->prev) f->prev->next = f->next;
            if (fifo_head == f) fifo_head = f->next;
            if (fifo_tail == f) fifo_tail = f->prev;
            fi_del(frame_inos, idx);

            DEBUG && fprintf(stderr, "Received ack for '%s'\n", f->fn);
            if (unlink(f->fn) < 0) {
                fprintf(stderr, "Unable to delete '%s'\n", f->fn);
                exit(1);
            }
            free(f->fn);
            free(f);
        }
        pthread_mutex_unlock(&frame_mutex);
    }
    return NULL;
}


int main(int argc, char **argv) {
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <path> <server> <port>\n", argv[0]);
        return 1;
    }
    // chdir, so we can be lazy and use relative paths everywhere.
    if (chdir(argv[1]) != 0) {
        fprintf(stderr, "Unable to enter '%s': %s\n", argv[1], strerror(errno));
        return 1;
    }

    // Sorry, only supports IPv4 for now.
    if (!inet_pton(AF_INET, argv[2], &servaddr.sin_addr)) {
        fprintf(stderr, "Invalid address '%s'\n", argv[2]);
        return 1;
    }
    servaddr.sin_port = htons(atoi(argv[3]));
    servaddr.sin_family = AF_INET;

    frame_inos = fi_init();

    if ((sock = socket(PF_INET, SOCK_SEQPACKET, IPPROTO_SCTP)) < 0) {
        fprintf(stderr, "Unable to create socket: %s\n", strerror(errno));
        return 1;
    }

    pthread_t pid;
    pthread_create(&pid, NULL, sender, NULL);
    pthread_create(&pid, NULL, receiver, NULL);

    for (;;) {
        scanframes();
        usleep(READDIR_INTERVAL_MSEC*1000);
        resetqueued();
    }

    return 0;
}
