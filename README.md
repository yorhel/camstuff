# Overall idea

A suckless way of handling the video feed of Raspberry Pi-based surveilance
cameras.

I'd like to keep the Pi's pretty dumb, just have them record images every
second and store-and-forward those to a central server, where they are further
processed for motion detection and encoded into timelapse videos.

Challenge: my WiFi is shit at 2.4GHz, the packet loss is too high for a naive
TCP-based solution to get enough bandwidth for reliable video transfer.

Challenge: I'd like to be able to watch the video stream in near-real time and
fast alerts from motion detection would be nice too, so frames should be
transferred with low latency when the WiFi allows for it.

Assumption: System time is somewhat synchronized between the Pi and the Server.

# Pi

The `pi` directory contains the code running on the Pi. It comes with two
services: camrecord, which is a wrapper around `libcamera-vid` to write JPEG
frames to a temporary directory, and camfwd, a little daemon to monitor the
directory for new frames and forwarding them to a server. Adjust the .service
files to your needs.

```sh
apt install libsctp1-dev
make install
systemd enable camrecord-$x.service
systemd enable camfwd-$x.service
```

The forwarder doesn't perform very well with long queues, it's designed for use
with a server that's more often online than not.

# Server

`camserv` receives the frames from the Pi and organizes them into a (UTC)
timestamp-based directory structure. Also provides a HTTP MJPEG stream on the
same port, which can be viewed in the browser or with
`mpv --profile=low-latency http://<address>:<port>/`.

To handle multiple Pis, run multiple server processes on different ports.

`camencode` monitors and reads the directory that `camserv` writes to and
creates daily timelapse videos with timestamps.

TODO:

- Do motion detection and provide some form of alerts (probably needs frames to
  be reordered, so may add some latency)
- Automatically clean up old JPEGs that have already been encoded into the
  timelapse video.

# Alternatives

(Motion)[https://motion-project.github.io/], obviously. But it doesn't support
libcamera (yet?) and the workarounds are awful. It also doesn't deal with
shitty WiFi very well and my timelapse videos are of higher quality. :)
